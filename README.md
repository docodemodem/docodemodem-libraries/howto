# どこでもでむライブラリ使い方

[まず最初に](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/1.%E3%81%BE%E3%81%9A%E6%9C%80%E5%88%9D%E3%81%AB)

[関数一覧](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/2.%E9%96%A2%E6%95%B0%E4%B8%80%E8%A6%A7)

[電源系](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/3.%E9%9B%BB%E6%BA%90%E7%B3%BB)

[コントロール系](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/4.%E3%82%B3%E3%83%B3%E3%83%88%E3%83%AD%E3%83%BC%E3%83%AB%E7%B3%BB)

[BEEP関連](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/5.BEEP%E9%96%A2%E9%80%A3)

[センサー関連](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/6.%E3%82%BB%E3%83%B3%E3%82%B5%E3%83%BC%E9%96%A2%E9%80%A3)

[AD_DA関連](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/7.AD_DA%E9%96%A2%E9%80%A3)

[RTC関連](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80/8.RTC%E9%96%A2%E9%80%A3)

# 特定小電力無線モデム用ライブラリ使い方

[まず最初に](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E7%89%B9%E5%AE%9A%E5%B0%8F%E9%9B%BB%E5%8A%9B%E7%84%A1%E7%B7%9A/1.%E3%81%BE%E3%81%9A%E6%9C%80%E5%88%9D%E3%81%AB)

[関数一覧](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E7%89%B9%E5%AE%9A%E5%B0%8F%E9%9B%BB%E5%8A%9B%E7%84%A1%E7%B7%9A/2.%E9%96%A2%E6%95%B0%E4%B8%80%E8%A6%A7)

[設定](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E7%89%B9%E5%AE%9A%E5%B0%8F%E9%9B%BB%E5%8A%9B%E7%84%A1%E7%B7%9A/3.%E8%A8%AD%E5%AE%9A)

[通信関連](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/%E7%89%B9%E5%AE%9A%E5%B0%8F%E9%9B%BB%E5%8A%9B%E7%84%A1%E7%B7%9A/4.%E9%80%9A%E4%BF%A1%E9%96%A2%E9%80%A3)

# LTE-Mモデム用ライブラリ使い方

[まず最初に](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/LTE_M/1.%E3%81%BE%E3%81%9A%E6%9C%80%E5%88%9D%E3%81%AB)

[関数一覧](https://gitlab.com/docodemodem/docodemodem-libraries/howto/-/wikis/LTE_M/2.%E4%BD%BF%E3%81%84%E6%96%B9)
